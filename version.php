<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version = 2023080401;
//$plugin->requires = TODO;
//$plugin->supported = TODO;   // Available as of Moodle 3.9.0 or later.
//$plugin->incompatible = TODO;   // Available as of Moodle 3.9.0 or later.
$plugin->component = 'filter_wkshkbk';
$plugin->maturity = MATURITY_STABLE;
$plugin->release = '4.0.5 (Build - 2019112600)';
